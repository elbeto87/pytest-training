from pytest import fixture
from selenium import webdriver
import json

data_location = '/home/egresia/Desktop/Otros/proyectos-py/pytest-training/tests/tv_brand.json'

@fixture(scope='function')
def chrome_browser():
    browser = webdriver.Chrome()
    yield browser
    print("Tear Down here")

def data_load(location):
    with open(location) as fileTv:
        data = json.load(fileTv)
        print(data)
    return data

@fixture(params=data_load(data_location))
def tv_brand(request):
    tv = request.param
    return tv


